package ru.t1.rydlev.tm.api;

import ru.t1.rydlev.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
