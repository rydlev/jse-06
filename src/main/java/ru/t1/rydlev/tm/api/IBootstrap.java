package ru.t1.rydlev.tm.api;

public interface IBootstrap {

    void run(String... args);

}
