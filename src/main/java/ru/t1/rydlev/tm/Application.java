package ru.t1.rydlev.tm;

import ru.t1.rydlev.tm.api.IBootstrap;
import ru.t1.rydlev.tm.component.Bootstrap;

public final class Application {

    public static void main(final String... args) {
        final IBootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}